var express = require('express');
var uuid = require('uuid/v1');
var converter = require('html-pdf');
var bodyParser = require('body-parser');

var app = express();
var options = { 
    format: 'Letter', 
    border: {
        top: "3cm",
        right: "2cm",
        bottom: "3cm",
        left: "2cm"
    }
};
var pathBase = 'C:\\Stampe\\';
var port = process.env.PORT || 3000;

app.use(bodyParser.text());
app.use(bodyParser.urlencoded({extended: true, limit: '50mb'}));

app.post('/ConvertToPdf', function(req, res) {
    var html = req.body;
    converter.create(html, options)
        .toStream(function(err, stream){
            if(err) return res.end(err.stack);
            res.setHeader('Content-type', 'application/pdf');
            stream.pipe(res);
        });
        // .toFile(completePath, function(err, res) {
        //     if (err) return console.log(err);
        //     console.log(res);
        // });  
});

app.listen(port);